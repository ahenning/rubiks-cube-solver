//solveSecondLayer.c, author Alex Henning, 4/15/2014
//solves the second layer of the cube

#include "cube_side.h"
#include <stdio.h>
#include <string.h>

//global array to hold the number of edges sovled, and which edges are solved
int edgesSolved[5] = {0, 0, 0, 0, 0};

//moves the top edge into the right edges place for the target side
void swapRightEdge(cube_side * side){
	rotateSideCW(side->top);
	rotateSideCW(side->right);
	rotateSideCC(side->top);
	rotateSideCC(side->right);
	rotateSideCC(side->top);
	rotateSideCC(side);
	rotateSideCW(side->top);
	rotateSideCW(side);
	printf("DO ONE SWAP EDGE RIGHT ON THE %s SIDE\n\n", side->color);
}

//moves the top edge into the left edges place for the target side
void swapLeftEdge(cube_side * side){
	rotateSideCC(side->top);
	rotateSideCC(side->left);
	rotateSideCW(side->top);
	rotateSideCW(side->left);
	rotateSideCW(side->top);
	rotateSideCW(side);
	rotateSideCC(side->top);
	rotateSideCC(side);
	printf("DO ONE SWAP EDGE LEFT ON THE %s SIDE\n\n", side->color);
	

}

//returns the number of edges that are solved in the second layer
void checkSolvedEdges(cube_side * side){
	int i;
	//reset edgesSolved
	for(i = 0; i < 5; i++){
		edgesSolved[i] = 0;
	}

	if((*side->top->home)[1][0] == 'b' && (*side->right->home)[1][2] == 'o'){
		edgesSolved[0]++;
		edgesSolved[1] = 1;
	}
	if((*side->top->home)[1][2] == 'b' && (*side->left->home)[1][0] == 'r'){
		edgesSolved[0]++;
		edgesSolved[2] = 1;
	}
	if((*side->bottom->home)[1][2] == 'g' && (*side->right->home)[1][0] == 'o'){
		edgesSolved[0]++;
		edgesSolved[3] = 1;
	}
	if((*side->bottom->home)[1][0] == 'g' && (*side->left->home)[1][2] == 'r'){
		edgesSolved[0]++;
		edgesSolved[4] = 1;
	}

}

//finishes the edge by calling swapEdge left/right
void finishEdge(cube_side * side, char cTop){

	if(cTop == (*side->left->home)[1][1]){
		swapLeftEdge(side);
	}
	else if(cTop == (*side->right->home)[1][1]){
		swapRightEdge(side);
	}
}

//moves the edge to the correct side and then calls the appropriate swap edge method
void moveEdge(cube_side * side, char c, char cTop){
	printf("\n");
	if(c == (*side->home)[1][1]){
		finishEdge(side, cTop);
	}
	else if(c == (*side->left->home)[1][1]){
		rotateSideCW(side->top);
		finishEdge(side->left, cTop);
	}	
	else if(c == (*side->right->home)[1][1]){
		rotateSideCC(side->top);
		finishEdge(side->right, cTop);
	}	
	else if(c == (*side->back->home)[1][1]){
		rotateSideCW(side->top);
		rotateSideCW(side->top);
		finishEdge(side->back, cTop);
	}	

		
}

//swaps a yellow edges with an unsolved edge when all top edges are yellow
void fixAllYellow(cube_side * side){
	//find an unsolved edge to swap out
	if(edgesSolved[1] == 0){
		swapLeftEdge(side->top);	
	}
	else if(edgesSolved[2] == 0){
		swapLeftEdge(side->left);	
	}
	else if(edgesSolved[3] == 0){
		swapLeftEdge(side->right);	
	}
	else if(edgesSolved[4] == 0){
		swapLeftEdge(side->bottom);	
	}
}

//finds an unsolved edge on the top side and solves it
void solveMiddleEdges(cube_side * side){
	int numYellows = 0;
	char c, cTop;
	
	checkSolvedEdges(side);
	
	while(edgesSolved[0] < 4){

		if((*side->home)[0][1] == 'y' || (*side->top->home)[0][1] == 'y'){
			numYellows++;
		}
		else{
			c = (*side->top->home)[0][1];
			cTop = (*side->home)[0][1];
			moveEdge(side->top, c, cTop);	
		}

		if((*side->home)[1][0] == 'y' || (*side->left->home)[0][1] == 'y'){
			numYellows++;
		}
		else{
			c = (*side->left->home)[0][1];
			cTop = (*side->home)[1][0];
			moveEdge(side->left, c, cTop);	
		}

		if((*side->home)[1][2] == 'y' || (*side->right->home)[0][1] == 'y'){
			numYellows++;
		}
		else{
			c = (*side->right->home)[0][1];
			cTop = (*side->home)[1][2];
			moveEdge(side->right, c, cTop);	
		}

		if((*side->home)[2][1] == 'y' || (*side->bottom->home)[0][1] == 'y'){
			numYellows++;
		}
		else{
			c = (*side->bottom->home)[0][1];
			cTop = (*side->home)[2][1];
			moveEdge(side->bottom, c, cTop);	
		}

		if(numYellows == 4){
			fixAllYellow(side);
		}
		numYellows = 0;
		checkSolvedEdges(side);
	}

	printf("\nThe second layer has been solved!\n\n");
	
}








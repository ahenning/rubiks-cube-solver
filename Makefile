CC = gcc
CFLAGS = -g -Wall
OBJECTS = rubiksSolver.o setUpCube.o solveCross.o solveCorners.o solveSecondLayer.o solveTopCross.o

rubiksSovler.exe: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o rubiksSolver.exe

%.o: %.c
	$(CC) $(CFLAGS) -c $<	 

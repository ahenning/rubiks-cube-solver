//solveTopCorners.c, author Alex Henning, 7/16/2014
//contains methods to solve the top corners of the cube.


#include "header.h"
#include "cube_side.h"
#include "corner_struct.h"
#include <stdio.h>
#include <string.h>

//moves around the corners on the top of the cube
void rearrangeTopCorners(cube_side * side){
	rotateSideCW(side->top);
	rotateSideCW(side->right);
	rotateSideCC(side->top);
	rotateSideCC(side->left);
	rotateSideCW(side->top);
	rotateSideCC(side->right);
	rotateSideCC(side->top);
	rotateSideCW(side->left);
	printf("DO A %s CORNER REARRANGE\n", side->color);
}



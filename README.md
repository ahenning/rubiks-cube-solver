Author: Alex Henning, 3/4/2014, 3x3 rubiks cube solver

Goal: Write a program which solves a rubiks cube and prints a set of step-by-step instructions for a user to follow. 

For the purpose of this program we must define a home position for the user to hold and view their cube from. (note, we refer to a side by the color of its middle). To be in the home position, the user should hold the cube with the green side facing toward them and the white side on the bottom. This will put the other colors as: yellow on top, red on left, orange on right, and blue on back.

This program stores each side of the cube using 3x3 char arrays. A color is denoted by
the first letter of its name (g for green, y for yellow, and so on...) While holding the cube
in its home position, the top left corner of the green side is green[0][0]. Keeping the yellow side facing upward, the top left corner of the orange, blue, and red sides is also the [0][0] cell. To view the yellow side properly, hold the 
cube in home position and then tilt the yellow top toward you. The top left corner of the yellow side
is yellow[0][0]. To view the white side properly, hold the cube in home position and tilt the white 
bottom toward you. The top left corner of the white side is white[0][0].
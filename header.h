//header.h, author Alex Henning, March 12, 2014

#ifndef HEADER_H
#define HEADER_H
/*initialize all cube sides in solved state*/

char greenSide [3][3] = {{'g', 'g', 'g'}, {'g', 'g', 'g'}, {'g', 'g', 'g'}};
char orangeSide [3][3] = {{'o', 'o', 'o'}, {'o', 'o', 'o'}, {'o', 'o', 'o'}};
char blueSide [3][3] = {{'b', 'b', 'b'}, {'b', 'b', 'b'}, {'b', 'b', 'b'}};
char redSide [3][3] = {{'r', 'r', 'r'}, {'r', 'r', 'r'}, {'r', 'r', 'r'}};
char yellowSide [3][3] = {{'y', 'y', 'y'}, {'y', 'y', 'y'}, {'y', 'y', 'y'}};
char whiteSide [3][3] = {{'w', 'w', 'w'}, {'w', 'w', 'w'}, {'w', 'w', 'w'}};


//declare cube sides
/*
char greenSide [3][3];
char orangeSide [3][3];
char blueSide [3][3];
char redSide [3][3];
char yellowSide [3][3];
char whiteSide [3][3];
*/


//setUpCube.c function declarations
void printSide(cube_side * side);
void printAllSides(cube_side * side);
int checkInputColor(char color);
void inputSideValues(cube_side * side);
void inputAllSideValues(cube_side * side);
void rotateSideCW(cube_side * side);
void rotateSideCC(cube_side * side);

//solveCross.c function declarations
int checkEdges(cube_side * side, char color);
void rotateOutEdge(cube_side * side, char color, int i, int j);
void rotateinEdge(cube_side * side, char color, int i, int j);
void checkBottomCrossArms(cube_side * side);
void checkSideCrossArms(cube_side * side, int i, int j);
void bringDownEdge(cube_side * side);
void solveCross(cube_side * side);

//solveCorners.c function declarations
void checkCorners(cube_side * side);
char *getCornerColors(char c1, char c2, char c3);
void rotateCorner(cube_side * side1, cube_side * side2);
void finishCorners(cube_side * home, cube_side * side1, cube_side * side2, int c);
void findBottomCorner(cube_side * side, char color);
void findTopCorner(cube_side * side, char color);
void solveBottomCorners(cube_side * side);

//solveSecondLayer.c function declarations
void swapRightEdge(cube_side * side);
void swapLeftEdge(cube_side * side);
void checkSolvedEdges(cube_side * side);
void finishEdge(cube_side * side, char cTop);
void moveEdge(cube_side * side, char c, char cTop);
void fixAllYellow(cube_side * side);
void solveMiddleEdges(cube_side * side);

//solveTopCross.c function declarations
void solveTopCross(cube_side * side);
void rearrangeTopCross(cube_side * side);



#endif

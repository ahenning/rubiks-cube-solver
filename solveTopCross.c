//solveTopCross.c, author alex henning, 4/18/2014
//solves the cross on top of the cube

#include "cube_side.h"
#include <stdio.h>
#include <string.h>

//rearranges the top cross arms
void rearrangeCrossArms(cube_side * side){
	printf("\n");
	rotateSideCW(side);
	rotateSideCW(side->top);
	rotateSideCC(side);
	rotateSideCW(side->top);
	rotateSideCW(side);
	rotateSideCW(side->top);
	rotateSideCW(side->top);
	rotateSideCC(side);
	printf("DO A %s CROSS ARM FIX\n", side->color);
}

//an algorithm to rearrange the colors on the top cross
void rearrangeTopCross(cube_side * side){
	printf("\n");
	rotateSideCW(side);
	rotateSideCW(side->right);
	rotateSideCW(side->top);
	rotateSideCC(side->right);
	rotateSideCC(side->top);
	rotateSideCC(side);
	printf("DO A CROSS REARRANGE WITH THE %s SIDE\n", side->color);
}

int fixTopCross(cube_side * side){
	int fixed = 0;
	while((*side->home)[0][1] != (*side->home)[1][1]){	
		rotateSideCW(side->top);
	}

	if((*side->right->home)[0][1] == (*side->right->home)[1][1]){
		rearrangeCrossArms(side);
		rotateSideCW(side->top);
		fixed = 1; 
	}	

	else if((*side->left->home)[0][1] == (*side->left->home)[1][1]){
		rearrangeCrossArms(side->left);
		rotateSideCW(side->top);
		fixed = 1;
	}	

	else if((*side->back->home)[0][1] == (*side->back->home)[1][1]){
		rearrangeCrossArms(side->right);
		rearrangeCrossArms(side);
		rotateSideCW(side->top);
		fixed = 1;
	}
	
	 return fixed;
		
	
}

//checks the state of the top cross and calls the rearrange method appropriately
void solveTopCross(cube_side * side){
	char c = (*side->home)[1][1];
	int fixed = 0;
	cube_side * currentSide = side->bottom;

	//make sure the cross is not already solved
	if((*side->home)[0][1] == c && (*side->home)[1][0] == c 
	 && (*side->home)[1][2] == c && (*side->home)[2][1] == c){
			//do nothing, its solved
		}
		
	else if((*side->home)[0][1] == c){
	
		if((*side->home)[1][0] == c){
			rearrangeTopCross(side->bottom);
			rearrangeTopCross(side->bottom);
		}
		
		else if((*side->home)[1][2] == c){
			rearrangeTopCross(side->left);
			rearrangeTopCross(side->left);
		}
		
		else if((*side->home)[2][1] == c){
			rearrangeTopCross(side->left);
		}
	}
	
	else if((*side->home)[1][0] == c){
	
		if((*side->home)[1][2] == c){
			rearrangeTopCross(side->bottom);
		}
		
		else if((*side->home)[1][2] == c){
			rearrangeTopCross(side->right);
			rearrangeTopCross(side->right);
		}
	}
	
	else if((*side->home)[1][2] == c && (*side->home)[2][1] == c){
		rearrangeTopCross(side->top);
		rearrangeTopCross(side->top);
	}
	
	// call fix top cross on each side until we can fix it
	while(fixed == 0){
		fixed = fixTopCross(currentSide);
		currentSide = currentSide->right;
	} 	
	
	printf("\nThe top cross is solved!\n");
}

/*setUpCube.c, Author Alex Henning, March 12, 2014
sets up the cube with basic functionality such as printing sides and side rotation
 contains printSide(), printAllSides(), checkInputColor(), rotateSideCW(), rotateSideCC()
*/
#include <stdio.h>
#include <string.h>
#include "cube_side.h"
#include "globals.h"

//global to count the number of moves taken
int numMoves = 0;

//prints the color values for one side of the cube for the user

void printSide(cube_side * side){
	int i, j;
	printf("The %s side:\n", side->color);
	for(i = 0; i<3; i++){
		for(j = 0; j<3; j++){
			if(j < 2){
				printf("%c, ", (*side->home)[i][j]);
			}
			else{
				printf("%c", (*side->home)[i][j]);
			}
		}
		printf("\n");
	}

}

//prints all the sides of the cube
void printAllSides(cube_side * side){
	printSide(side);
	printSide(side->right);
	printSide(side->back);
	printSide(side->left);
	printSide(side->top);
	printSide(side->bottom);
}

//checks to see if the user input a valid color for the side.
//valid colors include: g, o, b, r, y, or w
int checkInputColor(char color){
	if(color == 'g' || color == 'o' || color == 'b' || color == 'r' || color == 'y' || color == 'w'){
		return 1;
	}
	else return 0;
}


//allows the user to input the initial values for a specified side
//acceptable input values are: g, o, b, r, y, or w
void inputSideValues(cube_side * side){
	int i, j;
	int c = 0;
	char correct = 'n';
	char input = 'x';
	
	printf("Lets enter the %s side now.\n", side->color);
	while(correct != 'y'){
		for(i = 0; i < 3; i++){
			for(j = 0; j < 3; j++){
				while(!c){
					printf("Input a valid color for (%d, %d):", i, j);
					scanf(" %c", &input);
					c = checkInputColor(input);
				}
					(*side->home)[i][j] = input;
					c = 0;
				
			}
		}
		
		printSide(side);
		printf("Is this correct (y/n): ");
		scanf(" %c", &correct);
	}
}

//has a user input all the side values
void inputAllSideValues(cube_side * side){
	
	inputSideValues(side);
	inputSideValues(side->right);
	inputSideValues(side->back);
	inputSideValues(side->left);
	inputSideValues(side->top);
	inputSideValues(side->bottom);
}

//rotates a side of the cube clockwise
void rotateSideCW(cube_side * side){
	//temps
	char left, mid, right;

	//rotate the sides home face	
	mid = (*side->home)[0][1];	
	right = (*side->home)[0][2];	
	(*side->home)[0][2] = (*side->home)[0][0];	
	(*side->home)[0][1] = (*side->home)[1][0];	
	(*side->home)[0][0] = (*side->home)[2][0];	
	(*side->home)[1][0] = (*side->home)[2][1];	
	(*side->home)[2][0] = (*side->home)[2][2];	
	(*side->home)[2][1] = (*side->home)[1][2];	
	(*side->home)[2][2] = right;	
	(*side->home)[1][2] = mid;
	
	//update our other sides, unfortunately this is different for each side
	if(!strcmp(side->color, "green")){
//		printf("rotating green");
		left = (*side->top->home)[2][0];
		mid = (*side->top->home)[2][1];
		right = (*side->top->home)[2][2];
	
		(*side->top->home)[2][2] = (*side->left->home)[0][2];	
		(*side->top->home)[2][1] = (*side->left->home)[1][2];	
		(*side->top->home)[2][0] = (*side->left->home)[2][2];	
	
		(*side->left->home)[0][2] = (*side->bottom->home)[0][0];	
		(*side->left->home)[1][2] = (*side->bottom->home)[0][1];	
		(*side->left->home)[2][2] = (*side->bottom->home)[0][2];
	
		(*side->bottom->home)[0][0] = (*side->right->home)[2][0];	
		(*side->bottom->home)[0][1] = (*side->right->home)[1][0];	
		(*side->bottom->home)[0][2] = (*side->right->home)[0][0];	
		
		(*side->right->home)[0][0] = left;	
		(*side->right->home)[1][0] = mid;	
		(*side->right->home)[2][0] = right;		
	}

	else if(!strcmp(side->color, "orange")){
//		printf("rotating orange");
		
		left = (*side->top->home)[2][2];
		mid = (*side->top->home)[1][2];
		right = (*side->top->home)[0][2];
	
		(*side->top->home)[0][2] = (*side->left->home)[0][2];	
		(*side->top->home)[1][2] = (*side->left->home)[1][2];	
		(*side->top->home)[2][2] = (*side->left->home)[2][2];	
	
		(*side->left->home)[0][2] = (*side->bottom->home)[0][2];	
		(*side->left->home)[1][2] = (*side->bottom->home)[1][2];	
		(*side->left->home)[2][2] = (*side->bottom->home)[2][2];
	
		(*side->bottom->home)[0][2] = (*side->right->home)[2][0];	
		(*side->bottom->home)[1][2] = (*side->right->home)[1][0];	
		(*side->bottom->home)[2][2] = (*side->right->home)[0][0];	
		
		(*side->right->home)[0][0] = left;	
		(*side->right->home)[1][0] = mid;	
		(*side->right->home)[2][0] = right;	
	}

	else if(!strcmp(side->color, "blue")){
//		printf("rotating blue");
		left = (*side->top->home)[0][2];
		mid = (*side->top->home)[0][1];
		right = (*side->top->home)[0][0];
	
		(*side->top->home)[0][0] = (*side->left->home)[0][2];	
		(*side->top->home)[0][1] = (*side->left->home)[1][2];	
		(*side->top->home)[0][2] = (*side->left->home)[2][2];	
	
		(*side->left->home)[0][2] = (*side->bottom->home)[2][2];	
		(*side->left->home)[1][2] = (*side->bottom->home)[2][1];	
		(*side->left->home)[2][2] = (*side->bottom->home)[2][0];
	
		(*side->bottom->home)[2][0] = (*side->right->home)[0][0];	
		(*side->bottom->home)[2][1] = (*side->right->home)[1][0];	
		(*side->bottom->home)[2][2] = (*side->right->home)[2][0];	
		
		(*side->right->home)[0][0] = left;	
		(*side->right->home)[1][0] = mid;	
		(*side->right->home)[2][0] = right;	
	}
	else if(!strcmp(side->color, "red")){
//		printf("rotating red");
		left = (*side->top->home)[0][0];
		mid = (*side->top->home)[1][0];
		right = (*side->top->home)[2][0];
	
		(*side->top->home)[2][0] = (*side->left->home)[0][2];	
		(*side->top->home)[1][0] = (*side->left->home)[1][2];	
		(*side->top->home)[0][0] = (*side->left->home)[2][2];	
	
		(*side->left->home)[0][2] = (*side->bottom->home)[2][0];	
		(*side->left->home)[1][2] = (*side->bottom->home)[1][0];	
		(*side->left->home)[2][2] = (*side->bottom->home)[0][0];
	
		(*side->bottom->home)[0][0] = (*side->right->home)[0][0];	
		(*side->bottom->home)[1][0] = (*side->right->home)[1][0];	
		(*side->bottom->home)[2][0] = (*side->right->home)[2][0];	
		
		(*side->right->home)[0][0] = left;	
		(*side->right->home)[1][0] = mid;	
		(*side->right->home)[2][0] = right;	
	}
	else if(!strcmp(side->color, "yellow")){
//		printf("rotating yellow");
		left = (*side->top->home)[0][2];
		mid = (*side->top->home)[0][1];
		right = (*side->top->home)[0][0];
	
		(*side->top->home)[0][0] = (*side->left->home)[0][0];	
		(*side->top->home)[0][1] = (*side->left->home)[0][1];	
		(*side->top->home)[0][2] = (*side->left->home)[0][2];	
	
		(*side->left->home)[0][0] = (*side->bottom->home)[0][0];	
		(*side->left->home)[0][1] = (*side->bottom->home)[0][1];	
		(*side->left->home)[0][2] = (*side->bottom->home)[0][2];
	
		(*side->bottom->home)[0][0] = (*side->right->home)[0][0];	
		(*side->bottom->home)[0][1] = (*side->right->home)[0][1];	
		(*side->bottom->home)[0][2] = (*side->right->home)[0][2];	
		
		(*side->right->home)[0][0] = right;	
		(*side->right->home)[0][1] = mid;	
		(*side->right->home)[0][2] = left;	
	}
	else if(!strcmp(side->color, "white")){
//		printf("rotating white");
		left = (*side->top->home)[2][0];
		mid = (*side->top->home)[2][1];
		right = (*side->top->home)[2][2];
	
		(*side->top->home)[2][0] = (*side->left->home)[2][0];	
		(*side->top->home)[2][1] = (*side->left->home)[2][1];	
		(*side->top->home)[2][2] = (*side->left->home)[2][2];	
	
		(*side->left->home)[2][0] = (*side->bottom->home)[2][0];	
		(*side->left->home)[2][1] = (*side->bottom->home)[2][1];	
		(*side->left->home)[2][2] = (*side->bottom->home)[2][2];
	
		(*side->bottom->home)[2][0] = (*side->right->home)[2][0];	
		(*side->bottom->home)[2][1] = (*side->right->home)[2][1];	
		(*side->bottom->home)[2][2] = (*side->right->home)[2][2];	
		
		(*side->right->home)[2][0] = left;	
		(*side->right->home)[2][1] = mid;	
		(*side->right->home)[2][2] = right;	
	}

	numMoves++;
	printf("%d. rotate %s clockwise\n", numMoves, side->color);	
}



//rotates a side of the cube COUNTER clockwise
void rotateSideCC(cube_side * side){
	//temps
	char left, mid, right;

	//rotate the sides home face	
	mid = (*side->home)[0][1];	
	left = (*side->home)[0][0];	
	(*side->home)[0][0] = (*side->home)[0][2];	
	(*side->home)[0][1] = (*side->home)[1][2];	
	(*side->home)[0][2] = (*side->home)[2][2];	
	(*side->home)[1][2] = (*side->home)[2][1];	
	(*side->home)[2][2] = (*side->home)[2][0];	
	(*side->home)[2][1] = (*side->home)[1][0];	
	(*side->home)[2][0] = left;	
	(*side->home)[1][0] = mid;
	
	//update our other sides, unfortunately this is different for each side
	if(!strcmp(side->color, "green")){
//		printf("rotating green");
		left = (*side->top->home)[2][0];
		mid = (*side->top->home)[2][1];
		right = (*side->top->home)[2][2];
	
		(*side->top->home)[2][0] = (*side->right->home)[0][0];	
		(*side->top->home)[2][1] = (*side->right->home)[1][0];	
		(*side->top->home)[2][2] = (*side->right->home)[2][0];	
	
		(*side->right->home)[0][0] = (*side->bottom->home)[0][2];	
		(*side->right->home)[1][0] = (*side->bottom->home)[0][1];	
		(*side->right->home)[2][0] = (*side->bottom->home)[0][0];
	
		(*side->bottom->home)[0][0] = (*side->left->home)[0][2];	
		(*side->bottom->home)[0][1] = (*side->left->home)[1][2];	
		(*side->bottom->home)[0][2] = (*side->left->home)[2][2];	
		
		(*side->left->home)[0][2] = right;	
		(*side->left->home)[1][2] = mid;	
		(*side->left->home)[2][2] = left;		
	}

	else if(!strcmp(side->color, "orange")){
//		printf("rotating orange");
		left = (*side->top->home)[2][2];
		mid = (*side->top->home)[1][2];
		right = (*side->top->home)[0][2];
	
		(*side->top->home)[2][2] = (*side->right->home)[0][0];	
		(*side->top->home)[1][2] = (*side->right->home)[1][0];	
		(*side->top->home)[0][2] = (*side->right->home)[2][0];	
	
		(*side->right->home)[0][0] = (*side->bottom->home)[2][2];	
		(*side->right->home)[1][0] = (*side->bottom->home)[1][2];	
		(*side->right->home)[2][0] = (*side->bottom->home)[0][2];
	
		(*side->bottom->home)[0][2] = (*side->left->home)[0][2];	
		(*side->bottom->home)[1][2] = (*side->left->home)[1][2];	
		(*side->bottom->home)[2][2] = (*side->left->home)[2][2];	
		
		(*side->left->home)[0][2] = right;	
		(*side->left->home)[1][2] = mid;	
		(*side->left->home)[2][2] = left;		
	}

	else if(!strcmp(side->color, "blue")){
//		printf("rotating blue");
		left = (*side->top->home)[0][2];
		mid = (*side->top->home)[0][1];
		right = (*side->top->home)[0][0];
	
		(*side->top->home)[0][2] = (*side->right->home)[0][0];	
		(*side->top->home)[0][1] = (*side->right->home)[1][0];	
		(*side->top->home)[0][0] = (*side->right->home)[2][0];	
	
		(*side->right->home)[0][0] = (*side->bottom->home)[2][0];	
		(*side->right->home)[1][0] = (*side->bottom->home)[2][1];	
		(*side->right->home)[2][0] = (*side->bottom->home)[2][2];
	
		(*side->bottom->home)[2][0] = (*side->left->home)[2][2];	
		(*side->bottom->home)[2][1] = (*side->left->home)[1][2];	
		(*side->bottom->home)[2][2] = (*side->left->home)[0][2];	
		
		(*side->left->home)[0][2] = right;	
		(*side->left->home)[1][2] = mid;	
		(*side->left->home)[2][2] = left;		
	}

	else if(!strcmp(side->color, "red")){
//		printf("rotating red");
		left = (*side->top->home)[0][0];
		mid = (*side->top->home)[1][0];
		right = (*side->top->home)[2][0];
	
		(*side->top->home)[0][0] = (*side->right->home)[0][0];	
		(*side->top->home)[1][0] = (*side->right->home)[1][0];	
		(*side->top->home)[2][0] = (*side->right->home)[2][0];	
	
		(*side->right->home)[0][0] = (*side->bottom->home)[0][0];	
		(*side->right->home)[1][0] = (*side->bottom->home)[1][0];	
		(*side->right->home)[2][0] = (*side->bottom->home)[2][0];
	
		(*side->bottom->home)[0][0] = (*side->left->home)[2][2];	
		(*side->bottom->home)[1][0] = (*side->left->home)[1][2];	
		(*side->bottom->home)[2][0] = (*side->left->home)[0][2];	
		
		(*side->left->home)[0][2] = right;	
		(*side->left->home)[1][2] = mid;	
		(*side->left->home)[2][2] = left;		
	}

	else if(!strcmp(side->color, "yellow")){
//		printf("rotating yellow");
		left = (*side->top->home)[0][2];
		mid = (*side->top->home)[0][1];
		right = (*side->top->home)[0][0];
	
		(*side->top->home)[0][0] = (*side->right->home)[0][0];	
		(*side->top->home)[0][1] = (*side->right->home)[0][1];	
		(*side->top->home)[0][2] = (*side->right->home)[0][2];	
	
		(*side->right->home)[0][0] = (*side->bottom->home)[0][0];	
		(*side->right->home)[0][1] = (*side->bottom->home)[0][1];	
		(*side->right->home)[0][2] = (*side->bottom->home)[0][2];
	
		(*side->bottom->home)[0][0] = (*side->left->home)[0][0];	
		(*side->bottom->home)[0][1] = (*side->left->home)[0][1];	
		(*side->bottom->home)[0][2] = (*side->left->home)[0][2];	
		
		(*side->left->home)[0][0] = right;	
		(*side->left->home)[0][1] = mid;	
		(*side->left->home)[0][2] = left;		
	}

	else if(!strcmp(side->color, "white")){
//		printf("rotating white");
		left = (*side->top->home)[2][0];
		mid = (*side->top->home)[2][1];
		right = (*side->top->home)[2][2];
	
		(*side->top->home)[2][0] = (*side->right->home)[2][0];	
		(*side->top->home)[2][1] = (*side->right->home)[2][1];	
		(*side->top->home)[2][2] = (*side->right->home)[2][2];	
	
		(*side->right->home)[2][0] = (*side->bottom->home)[2][0];	
		(*side->right->home)[2][1] = (*side->bottom->home)[2][1];	
		(*side->right->home)[2][2] = (*side->bottom->home)[2][2];
	
		(*side->bottom->home)[2][0] = (*side->left->home)[2][0];	
		(*side->bottom->home)[2][1] = (*side->left->home)[2][1];	
		(*side->bottom->home)[2][2] = (*side->left->home)[2][2];	
		
		(*side->left->home)[2][0] = left;	
		(*side->left->home)[2][1] = mid;	
		(*side->left->home)[2][2] = right;		
	}

	

	numMoves++;
	printf("%d. rotate %s counter clockwise\n", numMoves, side->color);	

}




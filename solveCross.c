/* solveCross.c, author Alex Henning, 3/18/2014
This is step 1 in solving the cube
contains functions to solve the cross for the bottom of the cube
includes: checkEdges(), rotateOutEdge(), checkBottomCrossArms(), checkSideCrossArms, solveCross()
*/

#include "cube_side.h"
#include <stdio.h>
#include <string.h>

//checks how many edges on a side are of a certain color
//edges are side[0][1], side[1][0], side [1][2], side[2][1]
int checkEdges(cube_side * side, char color){
	int edges = 0;
	if((*side->home)[0][1] == color){
		edges++;
	}	
	
	if((*side->home)[1][0] == color){
		edges++;
	}
	
	if((*side->home)[1][2] == color){
		edges++;
	}
	
	if((*side->home)[2][1] == color){
		edges++;
	}
	printf("\n%d edges are on top.\n", edges);	
	return edges;

}

//rotates a side until a given color is not present in a target edge
void rotateOutEdge(cube_side * side, char color, int i, int j){

	while((*side->home)[i][j] == color){
		rotateSideCW(side);
	}
}

//rotates a side until a given color IS present in a target edge
void rotateInEdge(cube_side * side, char color, int i, int j){
	
	start:
	while((*side->home)[i][j] != color){
		rotateSideCW(side->top);
	}

	if(color == 'r' && (*side->top->home)[1][0] != 'w'){
		rotateSideCW(side->top);
		goto start;
	}
	else if(color == 'g' && (*side->top->home)[2][1] != 'w'){
		rotateSideCW(side->top);
		goto start;
	}
	else if(color == 'o' && (*side->top->home)[1][2] != 'w'){
		rotateSideCW(side->top);
		goto start;
	}
	else if(color == 'b' && (*side->top->home)[0][1] != 'w'){
		rotateSideCW(side->top);
		goto start;
	}
	
//	printf("rotatedIn %s edge.\n", side->color);
}

//checks for cross arms on the bottom, if one is found put it on the top side
void checkBottomCrossArms(cube_side * side){

	char color = (*side->home)[1][1];	

	if((*side->home)[0][1] == color){
		rotateOutEdge(side->back, color, 2, 1);
		rotateSideCW(side->top);
		rotateSideCW(side->top);
	}

	if((*side->home)[1][0] == color){

		rotateOutEdge(side->back, color, 1, 0);
		rotateSideCW(side->left);
		rotateSideCW(side->left);
	}	
	
	if((*side->home)[1][2] == color){
		rotateOutEdge(side->back, color, 1, 2);
		rotateSideCW(side->right);
		rotateSideCW(side->right);
	}
		
	if((*side->home)[2][1] == color){
		rotateOutEdge(side->back, color, 0, 1);
		rotateSideCW(side->bottom);
		rotateSideCW(side->bottom);
	}		
		
}	

//check for cross arms on a side, if one is found put it on the top
void checkSideCrossArms(cube_side * side, char color, int i, int j){
	
	if((*side->home)[1][0] == color){
		rotateOutEdge(side->top, color, i, i);
		rotateSideCW(side->top);
		rotateSideCC(side->left);
	}

	if((*side->home)[1][2] == color){

		rotateOutEdge(side->top, color, i, j);
		rotateSideCC(side->top);
		rotateSideCW(side->right);
	}	
	
	if((*side->home)[0][1] == color){
		rotateSideCW(side);
		rotateOutEdge(side->top, color, i, j);
		rotateSideCC(side->top);
		rotateSideCW(side->right);
	}
		
	if((*side->home)[2][1] == color){	
		rotateSideCC(side);
		rotateOutEdge(side->top, color, i, j);
		rotateSideCC(side->top);
		rotateSideCW(side->right);
	}		

}

/*the final step for solving the cross, puts all edges from the top
into the bottom in the correct places
*/
void bringDownEdge(cube_side * side){
	char color = (*side->home)[1][1];
	rotateInEdge(side, color, 0, 1);
	rotateSideCW(side);
	rotateSideCW(side);
}
	
//step 1. solve the "cross" for our white bottom.
void solveCross(cube_side * side){

	int armsOnTop = 0;
	char color = (*side->home)[1][1];
	//check how many cross arms are on top
	armsOnTop = checkEdges(side->back, color);
	
	//first we put all the cross arms on top, so while our top cross is not done...
	while(armsOnTop < 4){
		//check each side for cross arms
		checkBottomCrossArms(side);	
		checkSideCrossArms(side->top, color, 2, 1);
		checkSideCrossArms(side->right, color, 1, 2);
		checkSideCrossArms(side->bottom, color, 0, 1);
		checkSideCrossArms(side->left, color, 1, 0);
		armsOnTop = checkEdges(side->back, color);

	}
	printf("\nAll cross arms are on top.\n");

	//bring the cross arms to the bottom side in the correct places
	bringDownEdge(side->top);
	bringDownEdge(side->right);
	bringDownEdge(side->bottom);
	bringDownEdge(side->left);

	printf("\nThe cross is solved on the white side!\n");
		
}





//cube_side.h, author Alex Henning, March 14 2014

//struct to represent a side of the cube
//has pointers to the top, right, left, and bottom sides in relation to the home side

#ifndef SIDE_H
#define SIDE_H


typedef struct cube_side{
	
	char (*home)[3][3];
	struct cube_side *top;
	struct cube_side *left;
	struct cube_side *right;
	struct cube_side *bottom;
	struct cube_side *back;
	char color[7];
}cube_side;



#endif

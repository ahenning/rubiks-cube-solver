/*Author: Alex Henning, 3/4/2014, 3x3 rubiks cube solver*/

#include "cube_side.h"
#include "header.h"
#include <stdio.h>
#include <string.h>


int main(){
//cube side declarations and initializations	
	cube_side green;
	cube_side orange;
	cube_side blue;
	cube_side red;
	cube_side yellow;
	cube_side white;

	green.home = &greenSide;
	green.top = &yellow;
	green.right = &orange;
	green.left = &red;
	green.bottom = &white;
	green.back = &blue;
	strcpy(green.color, "green");
	
	orange.home = &orangeSide;
	orange.top = &yellow;
	orange.right = &blue;
	orange.left = &green;
	orange.bottom = &white;
	orange.back = &red;
	strcpy(orange.color, "orange");

	blue.home = &blueSide;
	blue.top = &yellow;
	blue.right = &red;
	blue.left = &orange;
	blue.bottom = &white;
	blue.back = &green;
	strcpy(blue.color, "blue");

	red.home = &redSide;
	red.top = &yellow;
	red.right = &green;
	red.left = &blue;
	red.bottom = &white;
	red.back = &orange;
	strcpy(red.color, "red");
	
	yellow.home = &yellowSide;
	yellow.top = &blue;
	yellow.right = &orange;
	yellow.left = &red;
	yellow.bottom = &green;
	yellow.back = &white;
	strcpy(yellow.color, "yellow");

	white.home = &whiteSide;
	white.top = &green;
	white.right = &orange;
	white.left = &red;
	white.bottom = &blue;
	white.back = &yellow;
	strcpy(white.color, "white");
	
	printf("\n");
	printf("Hello! Please input the cube sides to represent the cube you want to solve.\n ");
	printf("View the cube with the green side facing you and the yellow side on top.\n");
	printf("Please input the only the first letter of the color in each cell.\n");

	inputAllSideValues(&green);	
//	printAllSides(&green);
	solveCross(&white);
//	printAllSides(&green);
	solveBottomCorners(&white);
	solveMiddleEdges(&yellow);
	solveTopCross(&yellow);	
	printAllSides(&green);
	return 0;	
}


/* solveCorners.c, author alex henning, march 23 2014
contains methods to solve the corners of the cube
*/

#include "cube_side.h"
#include "globals.h"
#include "cornerStruct.h"
#include <stdio.h>
#include <string.h>

//global array to hold how many and which corners are solved
int corners[5] = {0, 0, 0, 0, 0,};

//checks how many corners are solved for a side. returns which and how many are solved.
void checkCorners(cube_side * side){
	char homeColor = (*side->home)[1][1];
	corners[0] = 0;
	
	if((*side->home)[0][0] == homeColor && (*side->top->home)[2][0] == (*side->top->home)[1][1]){
		corners[0]++;
		corners[1] = 1;
	}

	if((*side->home)[0][2] == homeColor && (*side->top->home)[2][2] == (*side->top->home)[1][1]){
		corners[0]++;
		corners[2] = 1;
	}

	if((*side->home)[2][0] == homeColor && (*side->bottom->home)[2][2] == (*side->bottom->home)[1][1]){
		corners[0]++;
		corners[3] = 1;
	}

	if((*side->home)[2][2] == homeColor && (*side->bottom->home)[2][0] == (*side->bottom->home)[1][1]){
		corners[0]++;
		corners[4] = 1;
	}
	
	//printf("\n%d corners are sovled.\n", corners[0]);
}

//determines the colors for a corner and returns them
struct corner_colors getCornerColors(char c1, char c2, char c3){
	corner_colors colors;
//	printf("%c%c%c\n", c1, c2, c3);
	if(c1 == 'g' || c2 == 'g' || c3 == 'g'){
		strcpy(colors.c, "g");
		
		if(c1 == 'r' || c2 == 'r' || c3 == 'r'){
			strcat(colors.c, "r");
		}	
		if(c1 == 'o' || c2 == 'o' || c3 == 'o'){
			strcat(colors.c, "o");
		}
	}

	else if(c1 == 'b' || c2 == 'b' || c3 == 'b'){
		strcpy(colors.c, "b");
			
		if(c1 == 'r' || c2 == 'r' || c3 == 'r'){
			strcat(colors.c, "r");
		}
		if(c1 == 'o' || c2 == 'o' || c3 == 'o'){
			strcat(colors.c, "o");
		}

	}
	printf("corner colors: %s\n", colors.c);
	return colors;
}

//rotates a corner
void rotateCorner(cube_side * side1, cube_side * side2){
	printf("\n");
	rotateSideCC(side1);
	rotateSideCC(side2);
	rotateSideCW(side1);
	rotateSideCW(side2);
	
}

//keep rotating a corner until it is sovled
void finishCorners(cube_side * home, cube_side * side1, cube_side * side2, int c){
	int counter = 0; 
	while(corners[c] == 0){
		counter++;
//		printf("\nDo a %s, %s corner rotation\n", side1->color, side2->color);
		rotateCorner(side1, side2);
		checkCorners(home);
	}
	printf("\n     DO %d %s, %s CORNER ROTATES.\n", counter, side1->color, side2->color);

}

//finds all out of place corners on the bottom side and solves them
int findBottomCorner(cube_side * side, char color){
//	printf("color: %c\n", color);
	char c1, c2, c3; 
	corner_colors colors;	

	if(corners[1] == 0 && ((*side->home)[0][0] == color
	  || (*side->top->home)[2][0] == color 
	  || (*side->left->home)[2][2] == color)){
//		printf("found corner 1 bottom\n");
		c1 = (*side->home)[0][0];
		c2 = (*side->top->home)[2][0];
		c3 = (*side->left->home)[2][2];
		rotateCorner(side->left, side->back);
		colors = getCornerColors(c1, c2, c3);
		
		if(!strcmp(colors.c, "gr")){
			finishCorners(side, side->left, side->back, 1);
		}	
		if(!strcmp(colors.c, "go")){
			rotateSideCC(side->back);
			finishCorners(side, side->top, side->back, 2);
		}
		if(!strcmp(colors.c, "br")){
			rotateSideCW(side->back);
			finishCorners(side, side->bottom, side->back, 3);
		}
		if(!strcmp(colors.c, "bo")){
			rotateSideCW(side->back);
			rotateSideCW(side->back);
			finishCorners(side, side->right, side->back, 4);

		}


		printf("Corner solved!\n\n");
		return 0;
	}
	else if(corners[2] == 0 && ((*side->home)[0][2] == color
	  || (*side->top->home)[2][2] == color 
	  || (*side->right->home)[2][0] == color)){
//		printf("found corner 2 bottom\n");
		c1 = (*side->home)[0][0];
		c2 = (*side->top->home)[2][2];
		c3 = (*side->right->home)[2][0];
		rotateCorner(side->top, side->back);
		colors = getCornerColors(c1, c2, c3);
		
		if(!strcmp(colors.c, "gr")){
			rotateSideCW(side->back);
			finishCorners(side, side->left, side->back, 1);
		}	
		if(!strcmp(colors.c, "go")){
			finishCorners(side, side->top, side->back, 2);
		}
		if(!strcmp(colors.c, "br")){
			rotateSideCW(side->back);
			rotateSideCW(side->back);
			finishCorners(side, side->bottom, side->back, 3);
		}
		if(!strcmp(colors.c, "bo")){
			rotateSideCC(side->back);
			finishCorners(side, side->right, side->back, 4);

		}

	
		printf("Corner solved!\n\n");
		return 0;
	}
	else if(corners[3] == 0 && ((*side->home)[2][0] == color
	  || (*side->bottom->home)[2][2] == color 
	  || (*side->left->home)[2][0] == color)){
//		printf("found corner 3 bottom\n");
		c1 = (*side->home)[2][0];
		c2 = (*side->bottom->home)[2][2];
		c3 = (*side->left->home)[2][0];
		rotateCorner(side->bottom, side->back);
		colors = getCornerColors(c1, c2, c3);
		
		if(!strcmp(colors.c, "gr")){
			rotateSideCC(side->back);
			finishCorners(side, side->left, side->back, 1);
		}	
		if(!strcmp(colors.c, "go")){
			rotateSideCC(side->back);
			rotateSideCC(side->back);
			finishCorners(side, side->top, side->back, 2);
		}
		if(!strcmp(colors.c, "br")){
			finishCorners(side, side->bottom, side->back, 3);
		}
		if(!strcmp(colors.c, "bo")){
			rotateSideCW(side->back);
			finishCorners(side, side->right, side->back, 4);

		}

		printf("Corner solved!\n\n");
		return 0;
	}
	else if(corners[4] == 0 && ((*side->home)[2][2] == color
	  || (*side->bottom->home)[2][0] == color 
	  || (*side->right->home)[2][2] == color)){
//		printf("found corner 4 bottom\n");
		c1 = (*side->home)[2][2];
		c2 = (*side->bottom->home)[2][0];
		c3 = (*side->right->home)[2][2];
		rotateCorner(side->right, side->back);
		colors = getCornerColors(c1, c2, c3);
	
		if(!strcmp(colors.c, "gr")){
			rotateSideCW(side->back);
			rotateSideCW(side->back);
			finishCorners(side, side->left, side->back, 1);
		}	
		if(!strcmp(colors.c, "go")){
			rotateSideCW(side->back);
			finishCorners(side, side->top, side->back, 2);
		}
		if(!strcmp(colors.c, "br")){
			rotateSideCC(side->back);
			finishCorners(side, side->bottom, side->back, 3);
		}
		if(!strcmp(colors.c, "bo")){
			finishCorners(side, side->right, side->back, 4);

		}

	
		return 0;
		printf("Corner solved!\n\n");
	}
	else{
		return 1;
	}
		
}


//finds an out of place corner on the top side and solves it
void findTopCorner(cube_side * side, char color){
	char c1, c2, c3; 
	corner_colors colors;	

	if((*side->home)[0][0] == color
	  || (*side->top->home)[0][2] == color 
	  || (*side->left->home)[0][0] == color){
//		printf("found corner 1 top\n");
		c1 = (*side->home)[0][0];
		c2 = (*side->top->home)[0][2];
		c3 = (*side->left->home)[0][0];
		colors = getCornerColors(c1, c2, c3);
		
		if(!strcmp(colors.c, "gr")){
			rotateSideCC(side);
			finishCorners(side->back, side->left, side, 1);
		}
		if(!strcmp(colors.c, "go")){
			rotateSideCC(side);
			rotateSideCC(side);
			finishCorners(side->back, side->bottom, side, 2);
		}
		if(!strcmp(colors.c, "br")){
			finishCorners(side->back, side->top, side, 3);
		}
		if(!strcmp(colors.c, "bo")){
			rotateSideCW(side);
			finishCorners(side->back, side->right, side, 4);

		}
		printf("Corner solved!\n\n");
	}
	else if((*side->home)[0][2] == color
	  || (*side->top->home)[0][0] == color 
	  || (*side->right->home)[0][2] == color){
//		printf("found corner 2 top\n");
		c1 = (*side->home)[0][2];
		c2 = (*side->top->home)[0][0];
		c3 = (*side->right->home)[0][2];
		colors = getCornerColors(c1, c2, c3);
		
		if(!strcmp(colors.c, "gr")){
			rotateSideCC(side);
			rotateSideCC(side);
			finishCorners(side->back, side->left, side, 1);
		}
		if(!strcmp(colors.c, "go")){
			rotateSideCW(side);
			finishCorners(side->back, side->bottom, side, 2);
		}
		if(!strcmp(colors.c, "br")){
			rotateSideCC(side);
			finishCorners(side->back, side->top, side, 3);
		}
		if(!strcmp(colors.c, "bo")){
			finishCorners(side->back, side->right, side, 4);

		}
		printf("Corner solved!\n\n");
	}
	else if((*side->home)[2][0] == color
	  || (*side->bottom->home)[0][0] == color 
	  || (*side->left->home)[0][2] == color){
//		printf("found corner 3 top\n");
		c1 = (*side->home)[2][0];
		c2 = (*side->bottom->home)[0][0];
		c3 = (*side->left->home)[0][2];
		colors = getCornerColors(c1, c2, c3);
		
		if(!strcmp(colors.c, "gr")){
			finishCorners(side->back, side->left, side, 1);
		}
		if(!strcmp(colors.c, "go")){
			rotateSideCC(side);
			finishCorners(side->back, side->bottom, side, 2);
		}
		if(!strcmp(colors.c, "br")){
			rotateSideCW(side);
			finishCorners(side->back, side->top, side, 3);
		}
		if(!strcmp(colors.c, "bo")){
			rotateSideCW(side);
			rotateSideCW(side);
			finishCorners(side->back, side->right, side, 4);

		}
		printf("Corner solved!\n\n");
	}	
	else if((*side->home)[2][2] == color
	  || (*side->bottom->home)[0][2] == color 
	  || (*side->right->home)[0][0] == color){
//		printf("found corner 4 top\n");
		c1 = (*side->home)[2][2];
		c2 = (*side->bottom->home)[0][2];
		c3 = (*side->right->home)[0][0];
		colors = getCornerColors(c1, c2, c3);
		
		if(!strcmp(colors.c, "gr")){
			rotateSideCW(side);
			finishCorners(side->back, side->left, side, 1);
		}
		if(!strcmp(colors.c, "go")){
			finishCorners(side->back, side->bottom, side, 2);
		}
		if(!strcmp(colors.c, "br")){
			rotateSideCW(side);
			rotateSideCW(side);
			finishCorners(side->back, side->top, side, 3);
		}
		if(!strcmp(colors.c, "bo")){
			rotateSideCC(side);
			finishCorners(side->back, side->right, side, 4);

		}
		printf("Corner solved!\n\n");
	}	
		
}

//step 2. solves the corners for the bottom layer
void solveBottomCorners(cube_side * side){
	
	int bottomDone = 0;
	checkCorners(side);
	
	while(corners[0] < 4){
		if(bottomDone == 0){
//			printf("calling findBotCorner\n");
			bottomDone = findBottomCorner(side, (*side->home)[1][1]);
		}
		else if(bottomDone == 1)
//			printf("calling findTopCorner\n");
			findTopCorner(side->back, (*side->home)[1][1]);	
	}
	printf("All corners solved! Bottom layer completed!!\n\n");	
}






